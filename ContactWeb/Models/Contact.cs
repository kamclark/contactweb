﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContactWeb.Models
{
    public class Contact
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string phonePrimary { get; set; }
        public string phoneSecondary { get; set; }
        public DateTime birthdate { get; set; }
        public string streetAddress1 { get; set; }
        public string streetAddress2 { get; set; }
        public string city { get; set; }
        public string State { get; set; }
        public string zipCode { get; set; }



    }
}